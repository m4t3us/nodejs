var models = require('./models');
var model =  new models.ClienteModel();


/* GET home page. */
var ClienteView = function(){
  
  this.get = function(req, res, next) {
    model.get(function(data){
      let context = { title: 'Clientes' , data: data };
      res.render( 'clientes', context );
    }, req.params.id)  
  }
  
}


module.exports = ClienteView;
