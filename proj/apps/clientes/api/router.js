var express = require('express');
var router = express.Router();
var clientesView = require('./clientes');
var view = new clientesView()


router.get( '/public/clientes',  view.get );
router.get( '/private/clientes/:id',  view.get );



module.exports = router;
