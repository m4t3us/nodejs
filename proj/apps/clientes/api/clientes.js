var models = require('../models');
var model =  new models.ClienteModel();


/* GET home page. */
var ClienteView = function(){
  
  this.get = function(req, res, next) {
    model.get(function(data){
      res.json( 
          data 
      );
    }, req.params.id)  
  }
  
}


module.exports = ClienteView;
