
var models = require('./models');

var model =  new models.ProdutoModel()


/* GET home page. */
var ProdutoView = function(){
  
  this.get = function(req, res, next) {
    model.get(function(data){
      let context = { title: 'Produtos' , data: data };
      res.render( 'produtos', context );
    })  
  }
  
}


module.exports = ProdutoView;
