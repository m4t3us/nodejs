var createError = require('http-errors');
var express = require('express');
var path = require('path');
var jwt = require('jsonwebtoken');
var cookieParser = require('cookie-parser');
var Keygrip = require('keygrip')
var logger = require('morgan');


var app = express();

//ROTAS RENDENRIZADAS
var clientesRouter = require('./apps/clientes/router');
var produtosRouter = require('./apps/produtos/router');

//ROTAS API
var apiAuthToken = require('./apps/api-auth-token/login')
var apiClientesRouter = require('./apps/clientes/api/router')

// view engine setup
app.set('views', path.join(__dirname, 'templates'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser({}))
app.use(express.static(path.join(__dirname, 'public')));


app.use( function(req, res, next){

    if(req.url.indexOf('/api/private') > -1){
        // Get auth header value
        const bearerHeader = req.headers['authorization'];
        // Check if bearer is undefined
        if(typeof bearerHeader !== 'undefined') {
          // Split at the space
          const bearer = bearerHeader.split(' ');
          // Get token from array
          const bearerToken = bearer[1];
          // Set the token
          req.token = bearerToken;
          // Next middleware
          next();
        } else {
          // Forbidden
          res.sendStatus(403);
        }
    }else next()
    
})


app.use( function(req, res, next) {

    if(req.url.indexOf('/api/private') > -1){
 
      jwt.verify(req.token, 'secretkey', (err, authData) => {
       
        if(err) {
          console.log('VerifyToken:', new Date().toLocaleString() + err );
          res.sendStatus(403);
        }else{
          console.log('VerifyToken:', new Date().toLocaleString() + authData );
          next()
        }

      });

    }else next()

});


app.use('/clientes', clientesRouter);
app.use('/produtos', produtosRouter);

app.use('/api/public/api-auth-token', apiAuthToken);
app.use('/api', apiClientesRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  
  
  if(req.url.indexOf('/api') > -1){
    res.json(
      {
      error : res.locals.message +  res.locals.error
      }   
    )
  }else res.render('error');
});



app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

module.exports = app;
